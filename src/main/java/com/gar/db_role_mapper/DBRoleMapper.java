/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gar.db_role_mapper;

import java.security.Principal;
import java.security.acl.Group;
import java.util.ArrayList;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;

import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.RoleMappingLoginModule;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBRoleMapper extends RoleMappingLoginModule {

  private static final Logger LOG = Logger.getLogger(DBRoleMapper.class.getName());

  private ArrayList<String> optionKeys;
  private Map<String, String> optionValues;

  // comment
  private static final String DATASOURCE = "datasource";
  private static final String ROLES_QUERY = "rolesQuery";
  private static final String ROLE_COLUMN = "roleColumn";

  public DBRoleMapper() {
    optionValues = new HashMap<>();
    optionKeys = new ArrayList<>();
    optionKeys.add(DATASOURCE);
    optionKeys.add(ROLES_QUERY);
    optionKeys.add(ROLE_COLUMN);
  }

  @Override
  public void initialize(Subject subject, CallbackHandler callbackHandler,
                         Map<String, ?> sharedState, Map<String, ?> options) {

    LOG.log(Level.INFO, "DRoMap Initializing");

    super.initialize(subject, callbackHandler, sharedState, options);

    String configPrefix = this.getClass().getPackage().getName() + ".";
    optionKeys.stream().filter(key -> (this.isBlank(optionValues.get(key)))).forEachOrdered(key -> {
      optionValues.put(key, System.getProperty(configPrefix + key));
    });

    LOG.log(Level.INFO, "DBRoMap Options read:{0}", options);
    LOG.log(Level.INFO, "DBRoMap Options parsed:{0}", optionValues);
    LOG.log(Level.INFO, "DBRoMap {0}:{1}", new String[]{DATASOURCE, optionValues.get(DATASOURCE)});
    LOG.log(Level.INFO, "DBRoMap {0}:{1}", new String[]{ROLES_QUERY, optionValues.get(ROLES_QUERY)});
    LOG.log(Level.INFO, "DBRoMap {0}:{1}", new String[]{ROLE_COLUMN, optionValues.get(ROLE_COLUMN)});
  }

  @Override
  protected Group[] getRoleSets() throws LoginException {

    boolean goon = true;

    if (goon && this.isBlank(optionValues.get(DATASOURCE))) {
      goon = false;
      LOG.log(Level.INFO, "DBRoMap ERROR DATASOURCE CONFIGURATION IS MISSING:{0}", optionValues.get(DATASOURCE));
    }

    if (goon && this.isBlank(optionValues.get(ROLES_QUERY))) {
      goon = false;
      LOG.log(Level.INFO, "DBRoMap ERROR ROLES_QUERY CONFIGURATION IS MISSING:{0}", optionValues.get(ROLES_QUERY));
    }

    if (goon && this.isBlank(optionValues.get(ROLE_COLUMN))) {
      goon = false;
      LOG.log(Level.INFO, "DBRoMap ERROR ROLE_COLUMN CONFIGURATION IS MISSING:{0}", optionValues.get(ROLE_COLUMN));
    }

    Group existingGroup = getExistingRolesFromSubject();
    String userName = super.getIdentity().getName();
    LOG.log(Level.INFO, "DBRoMap USER {0} EXISTING ROLES {1}", new String[]{userName, existingGroup.toString()});

    if (goon) {
      ArrayList<String> dbRoles = this.getRolesFromDB();
      try {

//        existingGroup.addMember(new SimplePrincipal("pare-naxeis"));
        dbRoles.forEach(role -> {
          existingGroup.addMember(new SimplePrincipal(role));
        });

      } catch (Exception e) {
        throw new LoginException("Failed to create group member for " + existingGroup);
      }
    }

    LOG.log(Level.INFO, "DBRoMap USER {0} UPDATED ROLES {1}", new String[]{userName, existingGroup.toString()});

    return new Group[]{existingGroup};
  }

  /**
   * Get existing roles for authenticated user
   *
   * @return existing roles or a an empty set
   */
  private Group getExistingRolesFromSubject() {
    Iterator<? extends Principal> iter = subject.getPrincipals().iterator();
    while (iter.hasNext()) {
      Principal p = iter.next();
      if (p instanceof Group) {
        Group g = (Group) p;
        if ("Roles".equals(g.getName())) {
          return g;
        }
      }
    }
    return new SimpleGroup("Roles");
  }

  /**
   * helper to check for existence of values
   *
   * @param str
   *
   * @return
   */
  public boolean isBlank(String str) {
    return (str == null) || (str.isEmpty()) || (str.length() == 0);
  }

  private ArrayList<String> getRolesFromDB() {
    ArrayList<String> authorityItemlist = new ArrayList<>();

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    //
    // - load authority items from DB
    //
    try {

      InitialContext ctx = new InitialContext();
      DataSource ds = (DataSource) ctx.lookup(optionValues.get(DATASOURCE));
      conn = ds.getConnection();
      ps = conn.prepareStatement(optionValues.get(ROLES_QUERY));
      rs = ps.executeQuery();
      while (rs.next()) {
        {
          authorityItemlist.add(rs.getString(rs.findColumn(optionValues.get(ROLE_COLUMN))));
        }
      }

      LOG.log(Level.INFO, "DBRoMap AUTHORITY ITEM LIST FROM DB IS:{0}", authorityItemlist);

    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException e) {
        }
      }
      if (ps != null) {
        try {
          ps.close();
        } catch (SQLException e) {
        }
      }
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException ex) {
        }
      }
    }
    return authorityItemlist;
  }

}
