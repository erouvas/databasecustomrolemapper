/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gar.db_role_mapper;

import java.util.Date;
import java.io.Serializable;
import java.util.Objects;

//
//CREATE TABLE `T_BUSINESS_GROUPS` (
//    `BUSINESS_GROUP_CODE` varchar(30) NOT NULL,
//    `COMPANY_CODE` VARCHAR(20) NOT NULL,
//    `EXPLANATION_TEXT` varchar(50) NOT NULL,
//    `STATUS` varchar(50) DEFAULT NULL,
//    `INSTANCE_ID` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//    `CREATE_USER_ID` varchar(20) DEFAULT NULL,
//    `REFERENCE_ID` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
//    `UPDATE_USER_ID` varchar(20) DEFAULT NULL
//  ) ENGINE=InnoDB DEFAULT CHARSET=utf8
//  
public class T_Business_Groups implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = -6111254699270304667L;

  private String BUSINESS_GROUP_CODE;
  private String COMPANY_CODE;
  private String EXPLANATION_TEXT;
  private String STATUS;
  private java.util.Date INSTANCE_ID;
  private String CREATE_USER_ID;
  private java.util.Date REFERENCE_ID;
  private String UPDATE_USER_ID;

  public T_Business_Groups() {
    super();
    // TODO Auto-generated constructor stub
  }

  public T_Business_Groups(String BUSINESS_GROUP_CODE, String COMPANY_CODE, String EXPLANATION_TEXT, String STATUS, Date INSTANCE_ID, String CREATE_USER_ID, Date REFERENCE_ID, String UPDATE_USER_ID) {
    this.BUSINESS_GROUP_CODE = BUSINESS_GROUP_CODE;
    this.COMPANY_CODE = COMPANY_CODE;
    this.EXPLANATION_TEXT = EXPLANATION_TEXT;
    this.STATUS = STATUS;
    this.INSTANCE_ID = INSTANCE_ID;
    this.CREATE_USER_ID = CREATE_USER_ID;
    this.REFERENCE_ID = REFERENCE_ID;
    this.UPDATE_USER_ID = UPDATE_USER_ID;
  }

  /**
   * Get the value of COMPANY_CODE
   *
   * @return the value of COMPANY_CODE
   */
  public String getCOMPANY_CODE() {
    return COMPANY_CODE;
  }

  /**
   * Set the value of COMPANY_CODE
   *
   * @param COMPANY_CODE new value of COMPANY_CODE
   */
  public void setCOMPANY_CODE(String COMPANY_CODE) {
    this.COMPANY_CODE = COMPANY_CODE;
  }

  /**
   * @return the bUSINESS_GROUP_CODE
   */
  public String getBUSINESS_GROUP_CODE() {
    return BUSINESS_GROUP_CODE;
  }

  /**
   * @return the eXPLANATION_TEXT
   */
  public String getEXPLANATION_TEXT() {
    return EXPLANATION_TEXT;
  }

  /**
   * @return the sTATUS
   */
  public String getSTATUS() {
    return STATUS;
  }

  /**
   * @return the iNSTANCE_ID
   */
  public java.util.Date getINSTANCE_ID() {
    return INSTANCE_ID;
  }

  /**
   * @return the cREATE_USER_ID
   */
  public String getCREATE_USER_ID() {
    return CREATE_USER_ID;
  }

  /**
   * @return the rEFERENCE_ID
   */
  public java.util.Date getREFERENCE_ID() {
    return REFERENCE_ID;
  }

  /**
   * @return the uPDATE_USER_ID
   */
  public String getUPDATE_USER_ID() {
    return UPDATE_USER_ID;
  }

  /**
   * @param bUSINESS_GROUP_CODE the bUSINESS_GROUP_CODE to set
   */
  public void setBUSINESS_GROUP_CODE(String bUSINESS_GROUP_CODE) {
    BUSINESS_GROUP_CODE = bUSINESS_GROUP_CODE;
  }

  /**
   * @param eXPLANATION_TEXT the eXPLANATION_TEXT to set
   */
  public void setEXPLANATION_TEXT(String eXPLANATION_TEXT) {
    EXPLANATION_TEXT = eXPLANATION_TEXT;
  }

  /**
   * @param sTATUS the sTATUS to set
   */
  public void setSTATUS(String sTATUS) {
    STATUS = sTATUS;
  }

  /**
   * @param iNSTANCE_ID the iNSTANCE_ID to set
   */
  public void setINSTANCE_ID(java.util.Date iNSTANCE_ID) {
    INSTANCE_ID = iNSTANCE_ID;
  }

  /**
   * @param cREATE_USER_ID the cREATE_USER_ID to set
   */
  public void setCREATE_USER_ID(String cREATE_USER_ID) {
    CREATE_USER_ID = cREATE_USER_ID;
  }

  /**
   * @param rEFERENCE_ID the rEFERENCE_ID to set
   */
  public void setREFERENCE_ID(java.util.Date rEFERENCE_ID) {
    REFERENCE_ID = rEFERENCE_ID;
  }

  /**
   * @param uPDATE_USER_ID the uPDATE_USER_ID to set
   */
  public void setUPDATE_USER_ID(String uPDATE_USER_ID) {
    UPDATE_USER_ID = uPDATE_USER_ID;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 29 * hash + Objects.hashCode(this.BUSINESS_GROUP_CODE);
    hash = 29 * hash + Objects.hashCode(this.COMPANY_CODE);
    hash = 29 * hash + Objects.hashCode(this.EXPLANATION_TEXT);
    hash = 29 * hash + Objects.hashCode(this.STATUS);
    hash = 29 * hash + Objects.hashCode(this.INSTANCE_ID);
    hash = 29 * hash + Objects.hashCode(this.CREATE_USER_ID);
    hash = 29 * hash + Objects.hashCode(this.REFERENCE_ID);
    hash = 29 * hash + Objects.hashCode(this.UPDATE_USER_ID);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final T_Business_Groups other = (T_Business_Groups) obj;
    if (!Objects.equals(this.BUSINESS_GROUP_CODE, other.BUSINESS_GROUP_CODE)) {
      return false;
    }
    if (!Objects.equals(this.COMPANY_CODE, other.COMPANY_CODE)) {
      return false;
    }
    if (!Objects.equals(this.EXPLANATION_TEXT, other.EXPLANATION_TEXT)) {
      return false;
    }
    if (!Objects.equals(this.STATUS, other.STATUS)) {
      return false;
    }
    if (!Objects.equals(this.CREATE_USER_ID, other.CREATE_USER_ID)) {
      return false;
    }
    if (!Objects.equals(this.UPDATE_USER_ID, other.UPDATE_USER_ID)) {
      return false;
    }
    if (!Objects.equals(this.INSTANCE_ID, other.INSTANCE_ID)) {
      return false;
    }
    if (!Objects.equals(this.REFERENCE_ID, other.REFERENCE_ID)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("T_Business_Groups{BUSINESS_GROUP_CODE=").append(BUSINESS_GROUP_CODE);
    sb.append(", COMPANY_CODE=").append(COMPANY_CODE);
    sb.append(", EXPLANATION_TEXT=").append(EXPLANATION_TEXT);
    sb.append(", STATUS=").append(STATUS);
    sb.append(", INSTANCE_ID=").append(INSTANCE_ID);
    sb.append(", CREATE_USER_ID=").append(CREATE_USER_ID);
    sb.append(", REFERENCE_ID=").append(REFERENCE_ID);
    sb.append(", UPDATE_USER_ID=").append(UPDATE_USER_ID);
    sb.append('}');
    return sb.toString();
  }

}
